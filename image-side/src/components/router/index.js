// 引入路由
import VueRouter from 'vue-router'
import Vue from 'vue';
import UserLogin from "@/components/views/login/UserLogin";
import FrontPage from "@/components/views/login/FrontPage";
import detailInfo from "@/components/views/info/DetailInfo";
import curveInfo from "@/components/views/info/CurveInfo";
import defaultPage from "@/components/views/info/DefaultPage";
import TevImageInfo from "@/components/views/info/TevImageInfo";
import WorkOrder from "@/components/views/info/WorkOrder";
import UploadDataModal from "@/components/views/info/modal/UploadDataModal";
import UploadTevInfoModal from "@/components/views/info/modal/UploadTevInfoModal";
import DownLoadPhoto from "@/components/views/info/DownLoadPhoto";
import CreateWorkOrder from "@/components/views/info/CreateWorkOrder";
import UserInfo from "@/components/views/info/UserInfo";
import DeviceInfo from "@/components/views/info/DeviceInfo";
import DingWarningInfo from "@/components/views/info/DingWarningInfo";
import AddDingGroup from "@/components/views/info/modal/AddDingGroup";
import SendDingWarning from "@/components/views/info/SendDingWarning";
import DingWarningRecord from "@/components/views/info/DingWarningRecord";

Vue.use(VueRouter);


const router = new VueRouter({
    mode: 'hash',
    routes: [
        {
            path: '/',
            name: 'login',
            component: UserLogin,
        },
        {
            path: '/front',
            name: 'front',
            component: FrontPage,
            redirect: '/defaultPage',
            children: [
                {
                    path: '/defaultPage',
                    name: 'defaultPage',
                    component: defaultPage,
                },
                {
                    path: '/detailInfo',
                    name: 'detailInfo',
                    component: detailInfo
                },
                {
                    path: '/curveInfo',
                    name: 'curveInfo',
                    component: curveInfo
                },
                {
                    path: '/tevImageInfo',
                    name: 'tevImageInfo',
                    component: TevImageInfo
                },
                {
                    path: '/workOrder',
                    name: 'workOrder',
                    component: WorkOrder
                },
                {
                    path: '/uploadData',
                    name: 'uploadData',
                    component: UploadDataModal
                },
                {
                    path: '/tevData',
                    name: 'tevData',
                    component: UploadTevInfoModal
                },
                {
                    path: '/downloadPhoto',
                    name: 'downloadPhoto',
                    component: DownLoadPhoto
                },
                {
                    path: '/createWorkOrder',
                    name: 'createWorkOrder',
                    component: CreateWorkOrder
                },
                {
                    path: '/userInfo',
                    name: 'userInfo',
                    component: UserInfo
                },
                {
                    path: '/deviceInfo',
                    name: 'deviceInfo',
                    component: DeviceInfo
                },
                {
                    path: '/messageWarning',
                    name: 'messageWarning',
                    component: DingWarningInfo
                },
                {
                    path: '/addGroup',
                    name: 'addGroup',
                    component: AddDingGroup
                },
                {
                    path: '/sendWarning',
                    name: 'sendWarning',
                    component: SendDingWarning
                },
                {
                    path: '/warningRecord',
                    name: 'warningRecord',
                    component: DingWarningRecord
                },
            ]
        }
    ]
})

export default router