import {postAction} from "@/components/api/action";
import moment from "moment";

export const PubMixins = {

    data() {
        return {
            queryParam : {},
            loading : false,
            datasource : [],
            pagination : {
                current : 1,
                pageSize : 10,
                total : 0,
                showTotal: (total, range) => {
                    return range[0] + "-" + range[1] + '总共' + total + '条';
                },
            }
        }
    },

    mounted() {
        this.searchQuery()
    },

    methods : {
        searchQuery() {
            this.loading = true
            this.queryParam.current = this.pagination.current
            this.queryParam.pageSize = this.pagination.pageSize
            console.log(JSON.stringify(this.queryParam))
            postAction(this.url.pageVo, JSON.stringify(this.queryParam))
                .then(res => {
                    this.datasource = res.data.data.records
                    this.pagination.total = res.data.data.total
                })
                .catch(e => this.$message.error(e))
                .finally(() => {
                    this.loading = false
                })
        },

        searchReset() {
            this.queryParam = {
                user: {},
                workLeader: {},
                device: {}
            }
            this.pagination = {
                current: 1,
                pageSize: 10,
                total: 0
            }
            this.searchQuery()
        },

        handleTableChange(page) {
            console.log(page)
            this.pagination.current = page.current
            this.searchQuery()
            console.log(this.pagination);
        },

    }
}
