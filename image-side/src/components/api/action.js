import axios from "axios";

// 创建 Axios 实例
export const instance = axios.create({
    // baseURL: '/api', // 设置基本的请求路径
});

export function postAction(url, parameter) {
    return instance({
        url: url,
        method: "post",
        data: parameter,
        headers: {
            "Content-Type": "application/json",
        },
    });
}
export function postForm(url, parameter) {
    let formData = new FormData();
    let keys = Object.keys(parameter);
    for (const key of keys) {
        formData.append(key, parameter[key]);
    }
    return instance({
        url: url,
        method: "post",
        data: formData,
    });
}

export function downloadFile(url, parameter) {
    let formData = new FormData();
    let keys = Object.keys(parameter);
    for (const key of keys) {
        formData.append(key, parameter[key]);
    }
    return instance({
        url: url,
        method: "post",
        data: formData,
        responseType : "blob"
    });
}

export function postFile(url, formData) {
    return instance({
        url: url,
        method: "post",
        data: formData,
        headers: {
            'Content-Type': 'multipart/form-data' // 设置正确的 Content-Type
        }
    });
}