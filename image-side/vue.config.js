const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  publicPath: './',
  transpileDependencies: true,
  lintOnSave:false,

  devServer: {
    port: 3000,
    proxy: {
      "/": {
        // target: "http://42.194.178.223:6677/identification/",
        target: "http://localhost:6677/identification/",
        ws: false,
        changeOrigin: true,
      },
    },
  },

})
